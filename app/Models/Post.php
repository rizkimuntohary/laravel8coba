<?php

namespace App\Models;


class Post  
{
    private static $blogPost = [
        [
            "title" => "Judul post pertama",
            "slug" => "judul-post-pertama",
            "author" => "Rizki Muntohary",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint distinctio praesentium vitae porro! Voluptatum temporibus minima a veritatis. Vero, odio, consequatur itaque, voluptatibus maxime laborum quasi at incidunt praesentium impedit veniam officiis sapiente obcaecati. Sit, quos! Alias reiciendis odio vitae? Libero nesciunt quod consectetur quia quaerat pariatur? Perferendis laborum accusamus possimus tempore nam architecto minima neque eaque sed at cum obcaecati id eius nihil magni, harum nesciunt natus numquam non! Totam quo quos laborum veniam dolorem facilis esse nulla voluptatem."
        ],
        [
            "title" => "Judul post kedua",
            "slug" => "judul-post-kedua",
            "author" => "Jhon",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint distinctio praesentium vitae porro! Voluptatum temporibus minima a veritatis. Vero, odio, consequatur itaque, voluptatibus maxime laborum quasi at incidunt praesentium impedit veniam officiis sapiente obcaecati. Sit, quos! Alias reiciendis odio vitae? Libero nesciunt quod consectetur quia quaerat pariatur? Perferendis laborum accusamus possimus tempore nam architecto minima neque eaque sed at cum obcaecati id eius nihil magni, harum nesciunt natus numquam non! Totam quo quos laborum veniam dolorem facilis esse nulla voluptatem."
        ],
    ];

    public static function all(){
        return collect(self::$blogPost);
    }

    public static function find($slug){
        $post = static::all();
        return $post->firstWhere('slug', $slug);

    }
}
