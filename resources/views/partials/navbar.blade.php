<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">Rizki Muntohary</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link {{ ($title === 'home') ? 'active' : '' }}" aria-current="page" href="/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ ($title === 'about') ? 'active' : '' }} " href="/about">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ ($title === 'blog') ? 'active' : '' }} " href="/blog">Blog</a>
        </li>
      </ul>
      <form class="d-flex">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success text-light" type="submit">Search</button>
      </form>
    </div>
  </div>
</nav>

Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint distinctio praesentium vitae porro! Voluptatum temporibus minima a veritatis. Vero, odio, consequatur itaque, voluptatibus maxime laborum quasi at incidunt praesentium impedit veniam officiis sapiente obcaecati. Sit, quos! Alias reiciendis odio vitae? Libero nesciunt quod consectetur quia quaerat pariatur? Perferendis laborum accusamus possimus tempore nam architecto minima neque eaque sed at cum obcaecati id eius nihil magni, harum nesciunt natus numquam non! Totam quo quos laborum veniam dolorem facilis esse nulla voluptatem.